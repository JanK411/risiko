package riskGame.gui;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import riskModel.Map;
import riskWorldEditor.gui.WorldEditorScreen;

public class MainScreenController {

    @FXML
    private ComboBox<Map> mapChooseComboBox;

    @FXML
    private Button abbrechenButton;

    @FXML
    private Button okayButton;

    @FXML
    GridPane playersGridPane;

    private int amountOfPlayers = 0;

    @FXML
    void okayAction() {

    }

    @FXML
    void abbrechenAction() {
        this.getStage().close();
    }

	private Stage getStage() {
		return (Stage) this.abbrechenButton.getScene().getWindow();
	}

    @FXML
    public void addPlayerAction() {
        final Label nameLabel = new Label("Name: ");
        this.playersGridPane.add(nameLabel, 0, this.amountOfPlayers);
        this.playersGridPane.add(new TextField(), 1, this.amountOfPlayers);
        this.playersGridPane.add(new ColorPicker(), 2, this.amountOfPlayers);
        this.getStage().sizeToScene();

        this.amountOfPlayers++;
    }

    @FXML
    public void createNewMapAction() throws Exception {
    	final WorldEditorScreen screen = new WorldEditorScreen();
    	screen.start(new Stage());
    	
    }

}
