package riskGame.gui;

import java.io.IOException;
import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainScreen extends Application {

    private MainScreenController controller;

    @Override
    public void start(final Stage primaryStage) throws IOException {
        final URL location = this.getClass().getResource("MainScreen.fxml");
        final FXMLLoader fxmlLoader = new FXMLLoader(location);

        final VBox root = (VBox) fxmlLoader.load();
        //        MainScreenController controller = fxmlLoader.getController();

        final Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        primaryStage.show();

    }

}
