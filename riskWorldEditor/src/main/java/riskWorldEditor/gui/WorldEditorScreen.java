package riskWorldEditor.gui;

import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class WorldEditorScreen extends Application {
	
	@Override
	public void start(final Stage primaryStage) throws Exception {
		final URL location = this.getClass().getResource("WorldEditorScreen.fxml");
		final FXMLLoader fxmlLoader = new FXMLLoader(location);
		
		final VBox root = (VBox) fxmlLoader.load();
		final WorldEditorScreenController controller = fxmlLoader.getController();
		controller.init();
		
		final Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.sizeToScene();
		primaryStage.setMinHeight(390);
		primaryStage.setMinWidth(480);
		primaryStage.show();
		
	}
	
}
