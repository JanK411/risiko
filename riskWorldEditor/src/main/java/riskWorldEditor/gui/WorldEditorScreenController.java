package riskWorldEditor.gui;

import de.jjt.framework.gui.ErrorDialog;
import de.jjt.framework.gui.RequestDialog;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import riskModel.Continent;
import riskModel.Land;
import riskModel.exceptions.RiskException;
import riskModel.gui.land.LandControl;
import riskModel.gui.land.observer.ConcreteObserveeWithConnectingEvent;
import riskModel.gui.land.observer.ConnectingEventVisitor;
import riskModel.gui.land.observer.ConnectingModeEvent;
import riskModel.gui.land.observer.ContinentModeEvent;
import riskModel.gui.land.observer.EditorModeEvent;
import riskModel.gui.land.observer.MoveModeEvent;
import riskModel.gui.land.observer.ObserveeWithConnectingEvent;
import riskModel.gui.land.observer.ObserverWithConnectingEvent;
import riskWorldEditor.WorldEditor;

public class WorldEditorScreenController implements ObserveeWithConnectingEvent {

	private final WorldEditor worldEditor;
	private final ToggleGroup tg = new ToggleGroup();

	private final ConcreteObserveeWithConnectingEvent concObservee;

	public WorldEditorScreenController() {
		this.worldEditor = WorldEditor.create();
		this.concObservee = new ConcreteObserveeWithConnectingEvent();
	}

	@FXML
	private Label infoLabel;

	@FXML
	private Pane worldPane;

	@FXML
	ToggleButton moveModeToggler;

	@FXML
	ToggleButton connectionModeToggler;

	@FXML
	ToggleButton continentModeToggler;

	@FXML
	Separator addititonalElementsSeperator;

	@FXML
	ComboBox<Continent> continentCombobox;

	@FXML
	Button addContinentButton;

	@FXML
	void keyPressedAction(final KeyEvent event) {
		if (event.isControlDown() && event.getCode().equals(KeyCode.S)) {
			this.saveAction();
		} else if (event.getCode().equals(KeyCode.N)) {
			this.addNewLandAction();
		} else if (event.getCode().equals(KeyCode.P)) {
			this.preferencesAction();
		}
	}

	@FXML
	void addNewLandAction() {
		final RequestDialog dialog = RequestDialog.create("Create new LandSkin", "Hallo, du bist echt super!");
		dialog.addTextField("name", "Name: ", "");
		dialog.addIntField("stars", "Stars: ", 1);
		if (dialog.getResult()) {
			final String name = dialog.getTextFieldValue("name");
			final Integer stars = dialog.getIntFieldValue("stars");
			try {
				final Land newLand = Land.createEmptyLand(name, stars);
				this.worldEditor.addNewLand(newLand);

				final LandControl control = LandControl.create(newLand);
				this.register(control);
				control.update(this.getConnectingEventType());
				this.worldPane.getChildren().add(control);
			} catch (final RiskException e) {
				ErrorDialog.create("Fehler", e.getMessage()).show();
			}
		}
	}

	@FXML
	public void addNewContinentAction() {
		final RequestDialog dialog = RequestDialog.create("Create new LandSkin", "Hallo, du bist echt super!");
		dialog.addTextField("name", "Name: ", "");
		dialog.addIntField("troopbonus", "Troopbonus: ", 1);
		// TODO framework-colorpicker?
		if (dialog.getResult()) {
			final String name = dialog.getTextFieldValue("name");
			final int troopbonus = dialog.getIntFieldValue("troopbonus");
			try {
				final Continent newContinent = Continent.createEmptyContinent(name, troopbonus);

				this.worldEditor.addNewContinent(newContinent);
				this.continentCombobox.getItems().add(newContinent);
				this.continentCombobox.setValue(newContinent);
				this.toggleVisibilityOfCombobox(true);
			} catch (final RiskException e) {
				ErrorDialog.create("Fehler", e.getMessage()).show();
			}
		}
	}

	@FXML
	void preferencesAction() {
		// TODO
	}

	@FXML
	void saveAction() {
		this.infoLabel.setText("Saving ...");
		// TODO hier speichern
		this.infoLabel.setText("Saved.");
	}

	@FXML
	void exitAction() {
		this.getStage().close();
	}

	private Stage getStage() {
		return (Stage) this.infoLabel.getScene().getWindow();
	}

	public void init() {

		this.connectionModeToggler.setToggleGroup(this.tg);
		this.moveModeToggler.setToggleGroup(this.tg);
		this.continentModeToggler.setToggleGroup(this.tg);

		this.moveModeToggler.setSelected(true);
		this.manageContinentRelatedVisbility(false);

		// TODO delete later
		this.worldPane.setStyle("-fx-background-color: #FFFFFF;");

		this.worldPane.heightProperty().addListener((observable, oldValue, newValue) -> {
			System.out.println("Y Value Changed (newValue: " + newValue.intValue() + ")");
		});
		this.worldPane.widthProperty().addListener((observable, oldValue, newValue) -> {
			System.out.println("X Value Changed (newValue: " + newValue.intValue() + ")");
		});

	}

	private void manageContinentRelatedVisbility(final boolean b) {
		this.addContinentButton.setVisible(b);
		this.addititonalElementsSeperator.setVisible(b);
		
		this.toggleVisibilityOfCombobox(b);
	}

	private void toggleVisibilityOfCombobox(final boolean b) {
		this.continentCombobox.setVisible(this.continentCombobox.getItems().isEmpty() ? false : b);
	}

	private EditorModeEvent getConnectingEventType() {
		if (this.tg.getSelectedToggle().equals(this.moveModeToggler)) {
			return MoveModeEvent.getInstance();
		} else if (this.tg.getSelectedToggle().equals(this.connectionModeToggler)) {
			return ConnectingModeEvent.getInstance();
		} else if (this.tg.getSelectedToggle().equals(this.continentModeToggler)) {
			return ContinentModeEvent.getInstance();
		}

		throw new RuntimeException("invalid Toggle " + this.tg.getSelectedToggle() + "!");
	}

	@FXML
	public void modeToggledAction() {
		final EditorModeEvent connectingEventType = this.getConnectingEventType();
		this.notifyObservers(connectingEventType);
		this.maybeToggleSeperator(connectingEventType);
	}

	private void maybeToggleSeperator(final EditorModeEvent editorModeEvent) {
		editorModeEvent.accept(new ConnectingEventVisitor() {

			@Override
			public void handle(final ContinentModeEvent continentModeEvent) {
				WorldEditorScreenController.this.manageContinentRelatedVisbility(true);
			}

			@Override
			public void handle(final ConnectingModeEvent connectingEnabledEvent) {
				WorldEditorScreenController.this.manageContinentRelatedVisbility(false);
			}

			@Override
			public void handle(final MoveModeEvent connectingDisabledEvent) {
				WorldEditorScreenController.this.manageContinentRelatedVisbility(false);
			}
		});
	}

	@Override
	public void register(final ObserverWithConnectingEvent o) {
		this.concObservee.register(o);
	}

	@Override
	public void deregister(final ObserverWithConnectingEvent o) {
		this.concObservee.deregister(o);
	}

	@Override
	public void notifyObservers(final EditorModeEvent connectingEvent) {
		this.concObservee.notifyObservers(connectingEvent);
	}

}
