package riskWorldEditor;

import riskWorldEditor.gui.WorldEditorScreen;

public class Main {
	public static void main(final String[] args) {
		javafx.application.Application.launch(WorldEditorScreen.class);
	}
}
