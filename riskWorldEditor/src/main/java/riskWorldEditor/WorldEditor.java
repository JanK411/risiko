package riskWorldEditor;

import riskModel.Continent;
import riskModel.ContinentSet;
import riskModel.Land;
import riskModel.LandSet;

public class WorldEditor {
	
	private final LandSet landSet;
	private final ContinentSet continentSet;
	
	private WorldEditor() {
		this.landSet = LandSet.create();
		this.continentSet = ContinentSet.create();
	}
	
	public static WorldEditor create() {
		return new WorldEditor();
	}
	
	public void addNewLand(final Land land) {
		this.landSet.addLand(land);
	}

	public void addNewContinent(final Continent continent) {
		this.continentSet.addContinent(continent);
	}
	
}
