package riskModel.exceptions;

/**
 * RuntimeException for Risk-Game
 */
public class RiskException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	public RiskException(final String string) {
		super(string);
	}
	
}
