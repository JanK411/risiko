package riskModel;

import java.util.Iterator;

import riskModel.exceptions.RiskException;

public class Continent {
    private final LandSet lands;

    private final String name;

    private final int troopBonus;

    private Continent(final LandSet lands, final String name, final int troopBonus) {
        this.lands = lands;
        this.name = name;
        this.troopBonus = troopBonus;
    }

    public static Continent createEmptyContinent(final String name, final int troopBonus) {
        return new Continent(LandSet.create(), name, troopBonus);
    }

    public Player getOccupier() {
        if (this.lands.isEmpty()) {
            throw new RiskException("Der Kontinent " + this.name + "ist leer!");
        }
        final Iterator<Land> iterator = this.lands.iterator();
        final Player possibleOccupier = iterator.next().getOwner();
        while (iterator.hasNext()) {
            final Player current = iterator.next().getOwner();
            if (current.equals(possibleOccupier) == false) {
                return Player.NO_PLAYER;
            }
        }
        return possibleOccupier;
    }

    public int getTroopBonus() {
        return this.troopBonus;
    }
    @Override
    public String toString() {
    	return this.name;
    }
}
