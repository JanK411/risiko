package riskModel;

import riskModel.exceptions.RiskException;

public class Connection {
	private final Land land1;
	private final Land land2;
	
	private Connection(final Land land1, final Land land2) {
		this.land1 = land1;
		this.land2 = land2;
	}
	
	public void addConnectedLandIfExists(final Land land, final LandSet ret) {
		if (this.land1.equals(land)) {
			ret.addLand(this.land2);
		} else if (this.land2.equals(land)) {
			ret.addLand(this.land1);
		}
	}
	
	static Connection addConnectionToManager(final Land land1, final Land land2) {
		if (land1.equals(land2)) {
			throw new RiskException("Es darf keine ConnectionControl zwischen ein und demselben Land bestehen!");
		}
		final Connection connection = new Connection(land1, land2);
		ConnectionManager.getInstance().addConnection(connection);
		return connection;
	}
	
	@Override
	public String toString() {
		return "<" + this.land1 + " - " + this.land2 + ">";
	}
	
}
