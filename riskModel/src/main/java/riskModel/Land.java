package riskModel;

import java.util.LinkedList;
import java.util.List;

import riskModel.exceptions.RiskException;

public class Land {
	private final String name;
	
	private final List<Troop> troops;
	
	private final Integer stars;
	
	private Land(final String name, final Integer stars, final List<Troop> troops) {
		this.name = name;
		this.stars = stars;
		this.troops = troops;
	}
	
	public static Land createEmptyLand(final String name, final Integer stars) {
		if (name.equals("") || name == null) {
			throw new RiskException("Der Name darf nicht leer sein!");
		}
		return new Land(name, stars, new LinkedList<Troop>());
	}
	
	public Player getOwner() {
		return this.troops.get(0).getOwner();
	}
	
	public String getName() {
		return this.name;
	}
	
	public LandSet getConnectedLands() {
		return ConnectionManager.getInstance().getConnectedLands(this);
	}
	
	public Connection addConnection(final Land land) {
		return ConnectionManager.getInstance().addConnection(this, land);
	}
	
	@Override
	public String toString() {
		return this.name;
	}
}
