package riskModel;

import java.util.Collection;

public class Player {
    public static final Player NO_PLAYER = new Player("This player is the Nullobject for player and as this it doesn't has a name", null, null);
    //FIXME initialisiere richtig!

    private final String name;

    private final Game game;

    private final Collection<Troop> troops;

    private Player(String name, Game game, Collection<Troop> troops) {
        this.name = name;
        this.game = game;
        this.troops = troops;
    }

    public static Player create(String name, Game game, Collection<Troop> troops) {
        return new Player(name, game, troops);
    }

    public int calculateTroopBonus() {
        return calculateContinentTroopBonus(); //TODO alle Truppenboni berechnen!
    }

    private int calculateContinentTroopBonus() {
        int troopBonus = 0;
        for (Continent current : this.game.getMap().getContinents()) {
            if (current.getOccupier().equals(this)) {
                troopBonus += current.getTroopBonus();
            }
        }
        return troopBonus;
    }
}
