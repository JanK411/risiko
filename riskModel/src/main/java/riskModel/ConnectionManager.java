package riskModel;

import java.util.LinkedList;

import riskModel.exceptions.RiskException;

class ConnectionManager {
	private final LinkedList<Connection> connections;
	
	private ConnectionManager() {
		this.connections = new LinkedList<>();
	};
	
	/**
	 * @param land1
	 * @param land2
	 * 
	 * @throws RiskException
	 */
	Connection addConnection(final Land land1, final Land land2) {
		if (this.connectionExists(land1, land2)) {
			throw new RiskException("Die ConnectionControl zwischen " + land1 + " und " + land2 + " existiert bereits!");
		}
		return Connection.addConnectionToManager(land1, land2);
	}
	
	private boolean connectionExists(final Land land1, final Land land2) {
		return land1.getConnectedLands().contains(land2);
	}
	
	LandSet getConnectedLands(final Land land) {
		final LandSet ret = LandSet.create();
		for (final Connection current : this.connections) {
			current.addConnectedLandIfExists(land, ret);
		}
		return ret;
	}
	
	void addConnection(final Connection connection) {
		this.connections.add(connection);
	}
	
	private static ConnectionManager instance;
	
	static ConnectionManager getInstance() {
		if (instance == null) {
			instance = new ConnectionManager();
		}
		return instance;
	}
	
	@Override
	public String toString() {
		return this.connections.toString();
	}
}
