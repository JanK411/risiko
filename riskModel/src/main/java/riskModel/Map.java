package riskModel;

import java.util.Collection;
import java.util.LinkedList;

public class Map {
    private final Collection<Continent> continents;

    private Map(Collection<Continent> continents) {
        this.continents = continents;
    }

    public static Map createEmptyMap() {
        return new Map(new LinkedList<Continent>());
    }

    public Collection<Continent> getContinents() {
        return this.continents;
    }
}
