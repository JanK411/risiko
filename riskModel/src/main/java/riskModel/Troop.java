package riskModel;

public abstract class Troop {
    private final Player owner;

    protected Troop(Player owner) {
        this.owner = owner;
    }

    public Player getOwner() {
        return this.owner;
    }

}
