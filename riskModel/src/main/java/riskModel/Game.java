package riskModel;

import java.util.Collection;

public class Game {

    private final Collection<Player> players;

    private final Map map;

    private Game(Map map, Collection<Player> players) {
        this.map = map;
        this.players = players;
    }

    public static Game create(Map map, Collection<Player> players) {
        return new Game(map, players);
    }

    public void addPlayer(Player player) {
        this.players.add(player);
    }

    /**
     * liest die Inizialen Truppen aus der Config-Datei aus und gibt diese zur�ck.
     * Die inizialen Truppen sind die Truppen, die jeder Spieler zur Verf�gung hat, um sie einsetzen zu k�nnen.
     * @return
     */
    private Collection<Troop> readInitTroops() {
        throw new UnsupportedOperationException("initTroops() implementieren!");
    }

    public Map getMap() {
        return this.map;
    }

}
