package riskModel.gui.land.editorState;

import de.jjt.framework.gui.RequestDialog;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;
import riskModel.clickedStates.ClickStateMemorizor;
import riskModel.exceptions.RiskException;
import riskModel.gui.connection.ConnectionControl;
import riskModel.gui.land.LandControl;

public class ConnectingState extends EditorMode {

	Line line = new Line();

	public ConnectingState(final LandControl landControl) {
		super(landControl);
	}

	@Override
	public void handleOnClick(final MouseEvent event, final LandControl control) {
		try {
			ClickStateMemorizor.getInstance().doStuffWith(control, this);
		} catch (final RiskException e) {
			// TODO Errordialog!
			final RequestDialog d = RequestDialog.create("Exception", e.getMessage());
			d.getResult();
		}
	}

	@Override
	public void handleOnDrag(final MouseEvent event, final LandControl control) {
		// doNothing
	}

	@Override
	public void handleMousePressed(final MouseEvent event, final LandControl control) {
	}

	@Override
	public void handleOnEnter(final MouseEvent event, final LandControl control) {
		// control.setCursor(Cursor.HAND);
	}

	@Override
	public void handleMouseReleased(final MouseEvent event, final LandControl control) {
	}

	public void handleLandMemorized(final LandControl memorizedLandControl, final LandControl control) {
		memorizedLandControl.deHighlight();
		if (memorizedLandControl.equals(control)) {
			System.out.println("equalt schon");
		} else {
			memorizedLandControl.getLand().addConnection(control.getLand());
			final ConnectionControl connectionControl = ConnectionControl.create(memorizedLandControl, control);
			final Pane worldPane = (Pane) control.getParent();
			worldPane.getChildren().add(connectionControl);
			connectionControl.toBack();
		}
	}

	public void handleNoLandMemorized(final LandControl control) {
		control.highlight();
		this.line.setStartX(control.getLayoutX());
		this.line.setStartY(control.getLayoutY());
	}

}
