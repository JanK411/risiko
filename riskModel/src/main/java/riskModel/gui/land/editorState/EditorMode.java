package riskModel.gui.land.editorState;

import javafx.scene.input.MouseEvent;
import riskModel.gui.land.LandControl;
import riskModel.gui.land.LandControlState;
import riskModel.gui.land.observer.ConnectingEventVisitor;
import riskModel.gui.land.observer.ConnectingModeEvent;
import riskModel.gui.land.observer.ContinentModeEvent;
import riskModel.gui.land.observer.EditorModeEvent;
import riskModel.gui.land.observer.MoveModeEvent;

public abstract class EditorMode implements LandControlState {

	private final LandControl landControl;

	public EditorMode(final LandControl landControl) {
		this.landControl = landControl;
	}

	@Override
	public void manageEventHandlers(final LandControl control) {
		control.setOnMouseClicked((event) -> {
			this.handleOnClick(event, control);
		});
		control.setOnMouseDragged((event) -> {
			this.handleOnDrag(event, control);
		});
		control.setOnMouseEntered((event) -> {
			this.handleOnEnter(event, control);
		});
		control.setOnMousePressed((event) -> {
			this.handleMousePressed(event, control);
		});
		control.setOnMouseReleased((event) -> {
			this.handleMouseReleased(event, control);
		});
	}

	protected LandControl getLandControl() {
		return this.landControl;
	}

	@Override
	public void handleStateChangeByIncomingConnectingEvent(final EditorModeEvent editorModeEvent) {
		editorModeEvent.accept(new ConnectingEventVisitor() {
			@Override
			public void handle(final ConnectingModeEvent connectingModeEvent) {
				System.out.println(connectingModeEvent);
				EditorMode.this.landControl.setState(new ConnectingState(EditorMode.this.landControl));
			}

			@Override
			public void handle(final MoveModeEvent moveModeEvent) {
				System.out.println(moveModeEvent);
				EditorMode.this.landControl.setState(new MoveLandState(EditorMode.this.landControl));
			}

			@Override
			public void handle(final ContinentModeEvent continentModeEvent) {
				System.out.println(continentModeEvent);
				EditorMode.this.landControl.setState(new ContinentEditingState(EditorMode.this.landControl));
			}
		});
	}

	protected abstract void handleOnClick(MouseEvent event, LandControl control);

	protected abstract void handleOnDrag(MouseEvent event, LandControl control);

	protected abstract void handleMousePressed(MouseEvent event, LandControl control);

	protected abstract void handleOnEnter(MouseEvent event, LandControl control);

	protected abstract void handleMouseReleased(MouseEvent event, LandControl control);

	// public static EditorMode create(final ConnectingEvent connectingEvent) {
	// return (EditorMode) connectingEvent.accept(new
	// ConnectingEventReturnVisitor() {
	//
	// @Override
	// public Object handle(final ConnectingEnabledEvent connectingEnabledEvent)
	// {
	// return new EditorMode(new ConnectingState());
	// }
	//
	// @Override
	// public Object handle(final ConnectingDisabledEvent
	// connectingDisabledEvent) {
	// return new EditorMode(new NotConnectingState());
	// }
	// });
	// }
}