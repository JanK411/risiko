package riskModel.gui.land.editorState;

import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;
import riskModel.gui.land.LandControl;

/**
 * DraggingState
 */
public class MoveLandState extends EditorMode {
	
	public MoveLandState(final LandControl landControl) {
		super(landControl);
	}
	
	final Delta dragDelta = new Delta();
	
	@Override
	public void handleOnClick(final MouseEvent event, final LandControl control) {
		System.out.println("clicked notconnecting");
	}
	
	@Override
	public void handleOnDrag(final MouseEvent event, final LandControl control) {
		control.setLayoutX(event.getSceneX() + this.dragDelta.x);
		control.setLayoutY(event.getSceneY() + this.dragDelta.y);
		control.setCursor(Cursor.CLOSED_HAND);
		control.notifyObservers();
	}
	
	@Override
	public void handleMousePressed(final MouseEvent event, final LandControl control) {
		this.dragDelta.x = control.getLayoutX() - event.getSceneX();
		this.dragDelta.y = control.getLayoutY() - event.getSceneY();
		control.setCursor(Cursor.CLOSED_HAND);
	}
	
	@Override
	public void handleOnEnter(final MouseEvent event, final LandControl control) {
		control.setCursor(Cursor.OPEN_HAND);
	}
	
	private class Delta {
		double x, y;
	}
	
	@Override
	public void handleMouseReleased(final MouseEvent event, final LandControl control) {
		control.setCursor(Cursor.OPEN_HAND);
	}
}