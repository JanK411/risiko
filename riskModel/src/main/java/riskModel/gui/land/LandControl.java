package riskModel.gui.land;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import riskModel.Land;
import riskModel.gui.connection.observer.ConcreteObservee;
import riskModel.gui.connection.observer.Observee;
import riskModel.gui.connection.observer.Observer;
import riskModel.gui.land.editorState.MoveLandState;
import riskModel.gui.land.observer.EditorModeEvent;
import riskModel.gui.land.observer.ObserverWithConnectingEvent;

public class LandControl extends Button implements Observee, ObserverWithConnectingEvent {
	
	private final Land land;
	
	private LandControlState state;
	
	private final ConcreteObservee concObservee;
	
	private LandControl(final Land land) {
		this.land = land;
		this.state = new MoveLandState(this);
		this.concObservee = new ConcreteObservee();
	}
	
	public static LandControl create(final Land land) {
		final LandControl ret = new LandControl(land);
		ret.setText(land.getName());
		ret.state.manageEventHandlers(ret);
		ret.setBackground(new Background(new BackgroundFill(Color.NAVAJOWHITE, new CornerRadii(42), Insets.EMPTY)));
		return ret;
	}
	
	public void highlight() {
		System.out.println("highlighted");
		final InnerShadow glow = new InnerShadow(12, Color.BLUE);
		this.setEffect(glow);
	}
	
	public void deHighlight() {
		System.out.println("deHighlighted");
		this.setEffect(null);
	}
	
	public Land getLand() {
		return this.land;
	}
	
	@Override
	public void register(final Observer o) {
		this.concObservee.register(o);
	}
	
	@Override
	public void deregister(final Observer o) {
		this.concObservee.deregister(o);
	}
	
	@Override
	public void notifyObservers() {
		this.concObservee.notifyObservers();
		
	}
	
	public void setState(final LandControlState state) {
		this.state = state;
	}
	
	@Override
	public void update(final EditorModeEvent connectingEvent) {
		this.state.handleStateChangeByIncomingConnectingEvent(connectingEvent);
		this.state.manageEventHandlers(this);
	}
	
}
