package riskModel.gui.land;

import riskModel.gui.land.observer.EditorModeEvent;

public interface LandControlState {
	
	void manageEventHandlers(LandControl control);
	
	void handleStateChangeByIncomingConnectingEvent(EditorModeEvent event);
	
}
