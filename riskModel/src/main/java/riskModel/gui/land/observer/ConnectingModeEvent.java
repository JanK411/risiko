package riskModel.gui.land.observer;

public class ConnectingModeEvent extends EditorModeEvent {
	
	private static ConnectingModeEvent instance;
	
	private ConnectingModeEvent() {
	}
	
	public static ConnectingModeEvent getInstance() {
		if (instance == null) {
			instance = new ConnectingModeEvent();
		}
		return instance;
	}
	
	@Override
	public void accept(final ConnectingEventVisitor v) {
		v.handle(this);
	}

	@Override
	public Object accept(final ConnectingEventReturnVisitor rv) {
		return rv.handle(this);
	}
	
}
