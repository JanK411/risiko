package riskModel.gui.land.observer;

public abstract class EditorModeEvent {
	public abstract void accept(ConnectingEventVisitor v);
	
	public abstract Object accept(ConnectingEventReturnVisitor rv);
}
