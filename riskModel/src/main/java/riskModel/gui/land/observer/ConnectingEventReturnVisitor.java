package riskModel.gui.land.observer;

public interface ConnectingEventReturnVisitor {

	Object handle(MoveModeEvent connectingDisabledEvent);

	Object handle(ConnectingModeEvent connectingEnabledEvent);

	Object handle(ContinentModeEvent continentModeEvent);
	
}
