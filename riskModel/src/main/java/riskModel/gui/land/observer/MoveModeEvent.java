package riskModel.gui.land.observer;

public class MoveModeEvent extends EditorModeEvent {
	
	private static MoveModeEvent instance;
	
	private MoveModeEvent() {
	}
	
	public static MoveModeEvent getInstance() {
		if (instance == null) {
			instance = new MoveModeEvent();
		}
		return instance;
	}
	
	@Override
	public void accept(final ConnectingEventVisitor v) {
		v.handle(this);
	}

	@Override
	public Object accept(final ConnectingEventReturnVisitor rv) {
		return rv.handle(this);
	}
	
}
