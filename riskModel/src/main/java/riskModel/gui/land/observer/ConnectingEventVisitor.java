package riskModel.gui.land.observer;

public interface ConnectingEventVisitor {

	void handle(MoveModeEvent connectingDisabledEvent);

	void handle(ConnectingModeEvent connectingEnabledEvent);

	void handle(ContinentModeEvent continentModeEvent);
	
}
