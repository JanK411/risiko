package riskModel.gui.land.observer;

public interface ObserveeWithConnectingEvent {
	public void register(final ObserverWithConnectingEvent o);
	
	public void deregister(final ObserverWithConnectingEvent o);
	
	void notifyObservers(final EditorModeEvent connectingEvent);
	
}
