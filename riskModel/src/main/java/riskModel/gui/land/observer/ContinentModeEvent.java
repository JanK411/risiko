package riskModel.gui.land.observer;

public class ContinentModeEvent extends EditorModeEvent {

private static ContinentModeEvent instance;
	
	private ContinentModeEvent() {
	}
	
	public static ContinentModeEvent getInstance() {
		if (instance == null) {
			instance = new ContinentModeEvent();
		}
		return instance;
	}
	
	@Override
	public void accept(final ConnectingEventVisitor v) {
		v.handle(this);
	}

	@Override
	public Object accept(final ConnectingEventReturnVisitor rv) {
		return rv.handle(this);
	}

}
