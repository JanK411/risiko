package riskModel.gui.land.observer;

import java.util.LinkedHashSet;
import java.util.Set;

public class ConcreteObserveeWithConnectingEvent implements ObserveeWithConnectingEvent {
	
	private final Set<ObserverWithConnectingEvent> observerWithConnectingEvents;
	
	public ConcreteObserveeWithConnectingEvent() {
		this.observerWithConnectingEvents = new LinkedHashSet<>();
	}
	
	@Override
	public void register(final ObserverWithConnectingEvent o) {
		this.observerWithConnectingEvents.add(o);
		
	}
	
	@Override
	public void deregister(final ObserverWithConnectingEvent o) {
		this.observerWithConnectingEvents.remove(o);
		
	}
	
	@Override
	public void notifyObservers(final EditorModeEvent connectingEvent) {
		for (final ObserverWithConnectingEvent current : this.observerWithConnectingEvents) {
			current.update(connectingEvent);
		}
	}
	
}
