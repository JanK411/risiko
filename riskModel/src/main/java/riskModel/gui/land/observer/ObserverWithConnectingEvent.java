package riskModel.gui.land.observer;

public interface ObserverWithConnectingEvent {

	void update(EditorModeEvent connectingEvent);
	
}
