package riskModel.gui.connection;

import javafx.scene.shape.Line;
import riskModel.gui.land.LandControl;

public class ConnectionControl extends Line {
	private final LandControl left;
	private final LandControl right;
	
	private ConnectionControl(final LandControl left, final LandControl right) {
		this.left = left;
		this.right = right;
	}
	
	public static ConnectionControl create(final LandControl left, final LandControl right) {
		final ConnectionControl ret = new ConnectionControl(left, right);
		ret.left.register(() -> ret.updateLeft());
		ret.right.register(() -> ret.updateRight());
		ret.updateLeft();
		ret.updateRight();
		
		return ret;
	}
	
	private double xOffset(final LandControl control) {
		return control.getWidth() / 2;
	}
	
	private double yOffset(final LandControl control) {
		return control.getHeight() / 2;
	}
	
	private void updateLeft() {
		this.setStartX(this.left.getLayoutX() + this.xOffset(this.left));
		this.setStartY(this.left.getLayoutY() + this.yOffset(this.left));
	}
	
	public void updateRight() {
		this.setEndX(this.right.getLayoutX() + this.xOffset(this.right));
		this.setEndY(this.right.getLayoutY() + this.yOffset(this.right));
	}
	
}
