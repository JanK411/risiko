package riskModel.gui.connection.observer;

public interface Observer {
	void update();
}
