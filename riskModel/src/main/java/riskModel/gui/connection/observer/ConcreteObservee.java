package riskModel.gui.connection.observer;

import java.util.LinkedHashSet;
import java.util.Set;

public class ConcreteObservee implements Observee {
	private final Set<Observer> observers;
	
	public ConcreteObservee() {
		this.observers = new LinkedHashSet<>();
	}
	
	@Override
	public void register(final Observer o) {
		this.observers.add(o);
		
	}
	
	@Override
	public void deregister(final Observer o) {
		this.observers.remove(o);
		
	}
	
	@Override
	public void notifyObservers() {
		for (final Observer current : this.observers) {
			current.update();
		}
	}
}
