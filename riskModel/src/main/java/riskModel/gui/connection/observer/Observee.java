package riskModel.gui.connection.observer;

public interface Observee {

	void register(Observer o);

	void deregister(Observer o);

	void notifyObservers();
	
}
