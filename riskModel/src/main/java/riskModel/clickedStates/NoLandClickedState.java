package riskModel.clickedStates;

import riskModel.gui.land.LandControl;
import riskModel.gui.land.editorState.ConnectingState;

public class NoLandClickedState extends ClickedState {
	
	private static ClickedState instance;
	
	private NoLandClickedState() {
	}
	
	public static ClickedState getInstance() {
		if (instance == null) {
			instance = new NoLandClickedState();
		}
		return instance;
	}
	
	@Override
	public void doStuffWith(final LandControl control, final ConnectingState connectingState) {
		connectingState.handleNoLandMemorized(control);
		ClickStateMemorizor.getInstance().setState(new LandClickedState(control));
		System.out.println("clicked state now set.");
	}
	
}
