package riskModel.clickedStates;

import riskModel.gui.land.LandControl;
import riskModel.gui.land.editorState.ConnectingState;

public abstract class ClickedState {
	
	public abstract void doStuffWith(final LandControl control, ConnectingState connectingState);
	
}
