package riskModel.clickedStates;

import riskModel.gui.land.LandControl;
import riskModel.gui.land.editorState.ConnectingState;

public class ClickStateMemorizor {
	private ClickedState state;
	
	private ClickStateMemorizor() {
		this.state = NoLandClickedState.getInstance();
	}
	
	private static ClickStateMemorizor instance;
	
	public static ClickStateMemorizor getInstance() {
		if (instance == null) {
			instance = new ClickStateMemorizor();
		}
		return instance;
	}
	
	public void doStuffWith(final LandControl control, final ConnectingState connectingState) {
		this.state.doStuffWith(control, connectingState);
	}
	
	public void setState(final ClickedState state) {
		this.state = state;
	}
}
