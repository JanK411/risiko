package riskModel.clickedStates;

import riskModel.gui.land.LandControl;
import riskModel.gui.land.editorState.ConnectingState;

public class LandClickedState extends ClickedState {
	
	private final LandControl memorizedLandControl;
	
	public LandClickedState(final LandControl control) {
		this.memorizedLandControl = control;
	}
	
	@Override
	public void doStuffWith(final LandControl control, final ConnectingState connectingState) {
		connectingState.handleLandMemorized(this.memorizedLandControl, control);
		ClickStateMemorizor.getInstance().setState(NoLandClickedState.getInstance());
	}
	
}
