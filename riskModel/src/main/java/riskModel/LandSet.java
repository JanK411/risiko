package riskModel;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class LandSet implements Iterable<Land> {
	private final Set<Land> lands;
	
	private LandSet(final Set<Land> lands) {
		this.lands = lands;
		
	}
	
	public static LandSet create() {
		return new LandSet(new LinkedHashSet<>());
	}
	
	public void addLand(final Land land) {
		this.lands.add(land);
	}
	
	public boolean contains(final Land land) {
		return this.lands.contains(land);
	}

	public boolean isEmpty() {
		return this.lands.isEmpty();
	}

	@Override
	public Iterator<Land> iterator() {
		return this.lands.iterator();
	}
	
}
