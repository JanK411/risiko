package riskModel;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class ContinentSet implements Iterable<Continent>{

	Set<Continent> continentSet;

	private ContinentSet() {
		this.continentSet = new LinkedHashSet<>();
	}
	
	public static ContinentSet create() {
		return new ContinentSet();
	}

	public void addContinent(final Continent continent) {
		this.continentSet.add(continent);
	}

	@Override
	public Iterator<Continent> iterator() {
		return this.continentSet.iterator();
	}
}
